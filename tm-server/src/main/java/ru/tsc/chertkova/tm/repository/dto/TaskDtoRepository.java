package ru.tsc.chertkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface TaskDtoRepository
        extends AbstractUserOwnerModelDtoRepository<TaskDTO> {

    @Nullable
    @Query("SELECT e FROM TaskDTO e WHERE e.userId=:userId AND e.projectId=:projectId")
    List<TaskDTO> findAllByProjectId(
            @Nullable @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId);

    @Modifying
    @Query("UPDATE TaskDTO e SET e.status=:status WHERE e.userId=:userId AND e.id=:id")
    void changeStatus(
            @NotNull @Param("id") String id,
            @NotNull @Param("userId") String userId,
            @NotNull @Param("status") String status);

    @Modifying
    @Query("UPDATE TaskDTO e SET e.projectId=:projectId WHERE e.userId=:userId AND e.id=:taskId")
    void bindTaskToProject(@NotNull @Param("taskId") String taskId,
                           @NotNull @Param("projectId") String projectId,
                           @NotNull @Param("userId") String userId);

    @Modifying
    @Query("DELETE FROM TaskDTO e WHERE e.userId=:userId")
    void clear(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("SELECT e FROM TaskDTO e WHERE e.userId=:userId")
    List<TaskDTO> findAll(@NotNull @Param("userId") String userId);

    @Nullable
    @Query("SELECT e FROM TaskDTO e WHERE e.id=:id AND e.userId=:userId")
    TaskDTO findById(@NotNull @Param("userId") String userId,
                     @NotNull @Param("id") String id);

    @Query("SELECT COUNT(e) FROM TaskDTO e WHERE e.userId=:userId")
    int getSize(@NotNull @Param("userId") String userId);

    @Modifying
    @Query("DELETE FROM TaskDTO e WHERE e.id=:id AND e.userId=:userId")
    void removeById(@NotNull @Param("userId") String userId,
                    @NotNull @Param("id") String id);

}
