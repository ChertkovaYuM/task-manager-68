package ru.tsc.chertkova.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

    @NotNull
    public static final String PATTERN = "yyyy-MM-dd";

    @NotNull
    public static SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    @Nullable
    @SneakyThrows
    public static Date toDate(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        return FORMATTER.parse(value);
    }

    @NotNull
    public static String toString(@Nullable final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}
