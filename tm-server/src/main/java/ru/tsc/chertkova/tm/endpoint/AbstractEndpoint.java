package ru.tsc.chertkova.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.tsc.chertkova.tm.api.service.*;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.exception.user.AccessDeniedException;
import ru.tsc.chertkova.tm.model.Session;

@Controller
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractEndpoint {

    @Getter
    @NotNull
    @Autowired
    private IAuthService authService;

    protected Session check(@Nullable final AbstractUserRequest request, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        @Nullable final Session session = getAuthService().validateToken(token);
        @Nullable final Role roleUser = session.getRole();
        final boolean check = roleUser == role;
        if (!check) throw new AccessDeniedException();
        return session;
    }

    protected Session check(@Nullable final AbstractUserRequest request) {
        if (request == null) throw new AccessDeniedException();
        @Nullable final String token = request.getToken();
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        return getAuthService().validateToken(token);
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return getPropertyService();
    }

}
