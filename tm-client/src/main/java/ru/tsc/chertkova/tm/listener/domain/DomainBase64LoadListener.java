package ru.tsc.chertkova.tm.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.dto.request.data.DataBase64LoadRequest;
import ru.tsc.chertkova.tm.enumerated.Role;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractDomainListener;

@Component
public final class DomainBase64LoadListener extends AbstractDomainListener {

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    public static final String DESCRIPTION = "Load date from base64 file.";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Nullable
    @Override
    public String argument() {
        return null;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@domainBase64LoadListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA LOAD BASE64]");
        getDomainEndpoint().loadDataBase64(new DataBase64LoadRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
