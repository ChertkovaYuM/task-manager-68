package ru.tsc.chertkova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.service.dto.IUserDtoService;
import ru.tsc.chertkova.tm.api.service.dto.IUserOwnerDtoService;
import ru.tsc.chertkova.tm.dto.model.AbstractUserOwnerModelDTO;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractUserOwnerDtoService<M extends AbstractUserOwnerModelDTO>
        extends AbstractDtoService<M> implements IUserOwnerDtoService<M> {

    @Nullable
    @Autowired
    protected IUserDtoService userService;

}
