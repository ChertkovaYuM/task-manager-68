package ru.tsc.chertkova.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.service.dto.IDtoService;
import ru.tsc.chertkova.tm.dto.model.AbstractModelDTO;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<M extends AbstractModelDTO> implements IDtoService<M> {

    @NotNull
    @Autowired
    protected ApplicationContext context;

}
