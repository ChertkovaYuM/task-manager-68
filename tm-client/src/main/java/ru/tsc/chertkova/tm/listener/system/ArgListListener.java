package ru.tsc.chertkova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.event.ConsoleEvent;
import ru.tsc.chertkova.tm.listener.AbstractListener;
import ru.tsc.chertkova.tm.listener.AbstractSystemListener;

import java.util.Collection;
import java.util.List;

@Component
public final class ArgListListener extends AbstractSystemListener {

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    public static final String DESCRIPTION = "Show arguments list.";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @Override
    @EventListener(condition = "@argListListener.command() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[ARGUMENTS]");
        System.out.println();
        for (@NotNull final AbstractListener listener : listeners) {
            if (listener.argument().isEmpty() || listener.argument() == null) continue;
            System.out.println(listener.argument());
        }
        System.out.println();
    }

}
