package ru.tsc.chertkova.tm.service;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.chertkova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['buildNumber']}")
    public String applicationVersion;

    @Value("#{environment['email']}")
    public String authorEmail;

    @Value("#{environment['developer']}")
    public String authorName;

    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @Value("#{environment['server.port']}")
    public Integer serverPort;

    @Value("#{environment['server.host']}")
    public String serverHost;

    @Value("#{environment['session.key']}")
    public String sessionKey;

    @Value("#{environment['session.timeout']}")
    public Integer sessionTimeout;

    @Value("#{environment['database.username']}")
    public String databaseUsername;

    @Value("#{environment['database.password']}")
    public String databasePassword;

    @Value("#{environment['database.url']}")
    public String databaseUrl;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.hbm2ddl']}")
    private String databaseHBM2DDL;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSQL;

    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondLvlCache;

    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseCacheRegionPrefix;

    @Value("#{environment['database.cache.provider_config']}")
    private String databaseCacheConfigFile;

    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseCacheFactoryClass;

}
