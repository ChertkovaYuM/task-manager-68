package ru.tsc.chertkova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.dto.model.UserDTO;
import ru.tsc.chertkova.tm.enumerated.Role;

@Repository
public interface UserDtoRepository extends AbstractDtoRepository<UserDTO> {

    @Nullable
    @Query("SELECT e FROM UserDTO e WHERE e.email=:email")
    UserDTO findByEmail(@NotNull @Param("email") String email);

    @Nullable
    @Query("SELECT e FROM UserDTO e WHERE e.login=:login")
    UserDTO findByLogin(@NotNull @Param("login") String login);

    @Modifying
    @Query("DELETE FROM UserDTO e WHERE e.login=:login")
    void removeByLogin(@NotNull @Param("login") String login);

    @Query("SELECT COUNT(e) FROM UserDTO e WHERE e.login=:login")
    int isLoginExist(@NotNull @Param("login") String login);

    @Query("SELECT COUNT(e) FROM UserDTO e WHERE e.email=:email")
    int isEmailExist(@NotNull @Param("email") String email);

    @Modifying
    @Query("UPDATE UserDTO e SET e.role=:role WHERE e.id=:id")
    void changeRole(@NotNull @Param("id") String id,
                    @NotNull @Param("role") Role role);

    @Modifying
    @Query("UPDATE UserDTO e SET e.passwordHash=:passwordHash WHERE e.id=:id")
    void setPassword(@NotNull @Param("id") String id,
                     @NotNull @Param("passwordHash") String passwordHash);

    @Modifying
    @Query("UPDATE UserDTO e SET e.locked=:locked WHERE e.login=:login")
    void setLockedFlag(@NotNull @Param("login") String login,
                       @NotNull @Param("locked") Boolean locked);

}
