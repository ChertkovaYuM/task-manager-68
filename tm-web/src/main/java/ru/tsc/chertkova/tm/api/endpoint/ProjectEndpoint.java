package ru.tsc.chertkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.model.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface ProjectEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<Project> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(
            @WebParam(name = "project", partName = "project")
            @NotNull @PathVariable("id") String id);

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "project", partName = "project")
            @NotNull @PathVariable("id") String id);

    @WebMethod
    @PostMapping("/save")
    Project save(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody Project project);

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody Project project);

    @WebMethod
    @PostMapping("/deleteAll")
    void clear(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody List<Project> projects);

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "project", partName = "project")
            @NotNull @PathVariable("id") String id);

    @WebMethod
    @GetMapping("/count")
    long count();

}
