package ru.tsc.chertkova.tm.component;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.chertkova.tm.api.service.IDomainService;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Getter
@Component
public final class Backup {

    @NotNull
    private final ScheduledExecutorService es
            = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    @Autowired
    private IDomainService domainService;

    public void start() {
        load();
        es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

    public void save() {
        getDomainService().saveDataBackup();
    }

    public void load() {
        getDomainService().loadDataBackup();
    }

}
