package ru.tsc.chertkova.tm.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsc.chertkova.tm.model.model.Project;

public interface IProjectRepository extends JpaRepository<Project, String> {

}
