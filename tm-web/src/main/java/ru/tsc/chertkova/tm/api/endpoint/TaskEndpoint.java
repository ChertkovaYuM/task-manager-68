package ru.tsc.chertkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.chertkova.tm.model.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/task")
public interface TaskEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @WebParam(name = "task", partName = "task")
            @NotNull @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "task", partName = "task")
            @NotNull @PathVariable("id") String id);

    @WebMethod
    @PostMapping("/save")
    Task save(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody Task task);

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody Task task);

    @WebMethod
    @PostMapping("/deleteAll")
    void clear(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody List<Task> tasks);

    @WebMethod
    @PostMapping("/clear")
    void clear();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "task", partName = "task")
            @NotNull @PathVariable("id") String id);

    @WebMethod
    @GetMapping("/count")
    long count();

}
